import redis from 'redis';
import { promisify } from 'util';
import config from 'config';

const { db, host, port } = config.get('database');

export const client = redis.createClient({
  host,
  port,
  db
});

export const lrangeAsync = promisify(client.lrange).bind(client);
// export const lremAsync = promisify(client.lrem).bind(client);
// export const lpushAsync = promisify(client.lpush).bind(client);

export const saddAsync = promisify(client.sadd).bind(client);
export const smenbersAsync = promisify(client.smembers).bind(client);

export const getAsync = promisify(client.get).bind(client);
export const setAsync = promisify(client.set).bind(client);

export const flushAllAsync = promisify(client.flushall).bind(client);
export const hmsetAsync = promisify(client.hmset).bind(client);
