import { ISystemError } from '../types';

const errors: { [key: number]: string } = {
  500: 'internal Server error',
  501: 'Not implemented',
  1001: 'Run params are not valid',
  2001: 'Dates params are not valid'
};

const getError = (code: number | null): { [key: string]: string } => {
  if (errors[code]) {
    return {
      code: `${code}`,
      message: errors[code]
    };
  }
  return {
    code: '500',
    message: errors[500]
  };
};

export const generateError = (
  status: number = 500,
  apiError?: number
): ISystemError => {
  const { code, message } = getError(apiError || status);
  const err: ISystemError = new Error(message);
  err.status = status;
  err.code = code;
  return err;
};

export const flatten = (arr: any[]): any[] => {
  function cal(element: any[], actual: any[]): any[] {
    return element.reduce((acc, e) => {
      e = Array.isArray(e) ? e : [e];
      return [...acc, ...e];
    }, actual);
  }

  return cal(arr, []);
};
