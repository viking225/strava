import createDebug from 'debug';
import { generateError, flatten } from '../helpers';
import { IRun, IRunStats, IRunService } from 'types';
import {
  client as redisClient,
  smenbersAsync,
  hmsetAsync,
  getAsync,
  saddAsync,
  setAsync
} from '../db';

// @ts-ignore
const debug = createDebug('strava:run:service');

export default class RunService implements IRunService {
  getRunSetName(startDate: string): string {
    if (!startDate || startDate.length === 0) {
      throw generateError(500);
    }

    const date = new Date(startDate);

    if (date.toString() === 'Invalid Date') {
      throw generateError(500);
    }
    return `run:hashset:${date.getFullYear()}-${date.getMonth() +
      1}-${date.getDate()}`;
  }

  async findRuns(startDateStr?: string, endDateStr?: string): Promise<IRun[]> {
    if (
      (startDateStr && new Date(startDateStr).toString() === 'Invalid Date') ||
      (endDateStr && new Date(endDateStr).toString() === 'Invalid Date')
    ) {
      throw generateError(500);
    }

    if (
      endDateStr &&
      startDateStr &&
      new Date(endDateStr) < new Date(startDateStr)
    ) {
      throw generateError(500);
    }
    // get run hashlist content
    const dateIndexes = await smenbersAsync('run:hashlist');

    // filter list of set
    const indexToRetrieve = dateIndexes.filter((index: string) => {
      const splitted = index.split('run:hashset:');
      if (splitted.length !== 2) {
        return false;
      }
      const date = new Date(`${splitted[1]} 00:00`);
      let isValid = startDateStr ? date >= new Date(startDateStr) : true;
      isValid = endDateStr ? isValid && date <= new Date(endDateStr) : isValid;
      return isValid;
    });

    // Get all runs from those set
    const getIdMembersPromise = new Promise<any>((resolve, reject) => {
      redisClient
        .multi(indexToRetrieve.map((index: string) => ['smembers', `${index}`]))
        .exec((err, r) => {
          if (err) reject(err);
          resolve(r);
        });
    });

    const result = await Promise.resolve(getIdMembersPromise);

    const getHashMembersPromise = new Promise<any>((resolve, reject) => {
      redisClient
        .multi(flatten(result).map((id: string) => ['hgetall', `run:${id}`]))
        .exec((err, r) => {
          if (err) reject(err);
          resolve(r);
        });
    });

    const rawData: IRun[] = await Promise.resolve(getHashMembersPromise);
    return rawData.map(
      (run: IRun): IRun => {
        return {
          ...run,
          kilometers: Number(run.kilometers),
          calories: Number(run.calories)
        };
      }
    );
  }

  generateRunStats(runs: IRun[]): IRunStats {
    if (runs.length === 0) {
      return {
        averageDistance: '0',
        averageCalories: '0'
      };
    }
    const runsStats = runs.reduce(
      (acc: number[], run: IRun) => {
        acc[0] = acc[0] + run.kilometers;
        acc[1] = acc[1] + run.calories;
        acc[2]++;
        return acc;
      },
      [0, 0, 0]
    );
    return {
      averageCalories: Number.parseFloat(
        `${runsStats[1] !== 0 ? runsStats[1] / runsStats[2] : 0}`
      ).toFixed(2),
      averageDistance: Number.parseFloat(
        `${runsStats[0] !== 0 ? runsStats[0] / runsStats[2] : 0}`
      ).toFixed(2)
    };
  }

  async saveRun(
    startDate: Date,
    endDate: Date,
    kilometers: number,
    calories: number
  ): Promise<IRun> {
    // get count to set index
    let count: number = (await getAsync('run:count')) || 0;
    count++;

    const daySet = this.getRunSetName(startDate.toString());

    // save hash
    await hmsetAsync(
      `run:${count}`,
      'startDate',
      startDate.toString(),
      'endDate',
      endDate.toString(),
      'kilometers',
      kilometers,
      'calories',
      calories
    );

    // add dayset in list
    await saddAsync('run:hashlist', daySet);
    // add count index in day set
    await saddAsync(daySet, count);
    // add new count
    await setAsync('run:count', `${count}`);

    return {
      kilometers,
      calories,
      startDate: startDate.toString(),
      endDate: endDate.toString()
    };
  }
}
