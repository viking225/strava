import RunService from '../run';
import { flushAllAsync, setAsync, hmsetAsync, saddAsync } from '../../db';
import runs from './run.mock';
// import createDebug from 'debug';

// const
// const debug = createDebug('strava:run:test');
const runService = new RunService();

describe('Run Service', () => {
  test('Service exist', done => {
    expect(runService).not.toBeUndefined();
    done();
  });

  describe('getRunSetName', () => {
    test('Should return an error if parameter is empty', done => {
      try {
        runService.getRunSetName('');
        expect(true).toBe(false);
      } catch (e) {
        expect(typeof e).toBe('object');
        expect(e.status).toBe(500);
      }
      done();
    });
    test('Should return an error if date is invalid', done => {
      try {
        runService.getRunSetName('2020-29-3');
        expect(true).toBe(false);
      } catch (err) {
        expect(typeof err).toBe('object');
        expect(err.status).toBe(500);
      }
      done();
    });
    test('Should return "run:hashset:2020-29-2" for date 2020-29-03', () => {
      const result: any = runService.getRunSetName('2020-03-29');
      expect(typeof result).toBe('string');
      expect(result).toBe('run:hashset:2020-3-29');
    });
  });

  beforeAll(async done => {
    //   empty test database
    await flushAllAsync();
    // insert database elements
    const runsPromises = runs.map(async (run, index) => {
      await hmsetAsync(
        `run:${index}`,
        'startDate',
        run[0],
        'endDate',
        run[1],
        'kilometers',
        run[2],
        'calories',
        run[3]
      );
      // add date index in list
      await saddAsync('run:hashlist', run[4]);

      // add key in index of day
      return saddAsync(run[4], `${index}`);
    });
    await Promise.all(runsPromises);
    // insert database count
    await setAsync('run:count', `${runs.length}`);
    done();
  });

  describe('findRuns', () => {
    test('Should return an array of Run w/o parameters', async done => {
      const result = await runService.findRuns();

      expect(Array.isArray(result)).toBeTruthy();
      expect(result.length).toBeGreaterThan(1);
      // Check mandatory fields
      const run = result[0];
      expect(
        run.calories && run.startDate && run.endDate && run.kilometers
      ).toBeTruthy();
      done();
    });
    test('Should return an error if startDate is invalid', async done => {
      try {
        await runService.findRuns('gilles');
        expect(true).toBe(false);
      } catch (err) {
        expect(typeof err).toBe('object');
        expect(err.status).toBe(500);
      }

      done();
    });
    test('Should return an error if endDate is invalid', async done => {
      try {
        await runService.findRuns('2018-3-2', 'pluslouche');
        expect(true).toBe(false);
      } catch (err) {
        expect(typeof err).toBe('object');
        expect(err.status).toBe(500);
      }
      done();
    });
    test('Should return an error if startDate and enDate exists but startDate > endDate', async done => {
      try {
        await runService.findRuns('2018-3-2', '2018-3-1');
        expect(true).toBe(false);
      } catch (err) {
        expect(typeof err).toBe('object');
        expect(err.status).toBe(500);
      }
      done();
    });
    test('Should return empty array of runs if date is valid', async done => {
      const result = await runService.findRuns('2020-03-29', '2020-04-30');
      expect(Array.isArray(result)).toBeTruthy();
      // Must be 3 cause of mock
      expect(result.length).toBe(3);
      done();
    });
  });

  describe('isValidRun', () => {
    test.todo('Should return false if startDate is invalid');
    test.todo('Should return false if endDate is invalid');
    test.todo('Should return false if burnt calories is invalid');

    test.todo('Should return true if valid parameters');
  });

  describe('generateRunStats', () => {
    test('Should return null values when params is empty array', async done => {
      const result = await runService.generateRunStats([]);
      expect(typeof result).toBe('object');
      expect(result.averageCalories && result.averageDistance).toBeTruthy();
      done();
    });
    test('Should return an valid stat object with correct parameters', async done => {
      const result = await runService.generateRunStats([
        {
          startDate: '2019-02-03 11:00',
          endDate: '2019-02-03 11:15',
          kilometers: 10,
          calories: 200
        },
        {
          startDate: '2019-02-03 19:00',
          endDate: '2019-02-03 19:45',
          kilometers: 10,
          calories: 200
        }
      ]);
      expect(typeof result).toBe('object');
      expect(result.averageCalories && result.averageDistance).toBeTruthy();
      expect(Number(result.averageDistance) === 10).toBeTruthy();
      expect(Number(result.averageCalories)).toBeGreaterThan(0);
      done();
    });
  });
});
