import {
  Application,
  Router,
  Response,
  Request,
  NextFunction
} from 'express-serve-static-core';

// App
interface IAppExpress extends Application {}
interface IAppRouteur extends Router {}
interface IAppResponse extends Response {}
interface IAppNext extends NextFunction {}

interface IAppRequest extends Request {
  app: IAppExpress;
}

interface IApp {
  routeur: IAppRouteur;
  express: IAppExpress;
}

interface ISystemError extends Error {
  address?: string;
  code?: string | number;
  dest?: string;
  errno?: number | string;
  info?: object;
  message: string;
  path?: string;
  port?: number;
  syscall?: string;
  status?: number;
}

interface IAppService {
  run: IRunService;
}

// Run
interface IRun {
  startDate: string;
  endDate: string;
  kilometers: number;
  calories: number;
}

interface IRunService {
  saveRun(
    startDate: Date,
    endDate: Date,
    kilometers: number,
    calories: number
  ): Promise<IRun>;
  generateRunStats(runs: IRun[]): IRunStats;
  findRuns(startDateStr?: string, endDateStr?: string): Promise<IRun[]>;
  getRunSetName(startDate: string): string;
}

interface IRunStats {
  averageDistance: string;
  averageCalories: string;
}

export {
  ISystemError,
  IApp,
  IAppRouteur,
  IAppExpress,
  IAppResponse,
  IAppRequest,
  IAppNext,
  IRun,
  IRunStats,
  IRunService,
  IAppService
};
