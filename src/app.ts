import express from 'express';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import createStatsRoutes from './routes/stats';
import createRunRoutes from './routes/run';
import { indexRoute, errorMiddleware, notFoundMiddleware } from './routes';
import { IAppRouteur } from './types';
import RunService from './services/run';

class App {
  routeur: IAppRouteur;
  express: any;

  constructor() {
    this.express = express();
    this.routeur = express.Router();

    // implements routes
    const statsRoutes = createStatsRoutes(this.routeur);
    const runRoutes = createRunRoutes(this.routeur);

    this.express.use(logger('dev'));
    this.express.use(express.json());
    this.express.use(express.urlencoded({ extended: false }));
    this.express.use(cookieParser());

    this.express.set('services', {
      run: new RunService()
    });

    // use routes
    this.express.use('/stats', statsRoutes);
    this.express.use('/run', runRoutes);
    this.express.get('/', indexRoute);
    // not found
    this.express.use(notFoundMiddleware);
    // error
    this.express.use(errorMiddleware);
  }
}

export default new App().express;
