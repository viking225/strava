import createDebug from 'debug';
import {
  IAppRouteur,
  IAppRequest,
  IAppResponse,
  IAppNext,
  IAppService
} from '../types';
import { generateError } from '../helpers';

const debug = createDebug('strava:route:run');

export default (routeur: IAppRouteur): IAppRouteur => {
  routeur.post(
    '/',
    async (req: IAppRequest, res: IAppResponse, next: IAppNext) => {
      const body = req.body;
      // check valid params
      debug(body);
      let startDate = null;
      let endDate = null;
      let km = 0;
      let calories = 0;

      let isValidRequest =
        !body.startDate ||
        !body.endDate ||
        (body.kilometers === undefined && body.calories === undefined)
          ? false
          : true;

      if (isValidRequest) {
        startDate = new Date(body.startDate);
        endDate = new Date(body.endDate);
        km = Number(body.kilometers);
        calories = Number(body.calories);

        isValidRequest =
          startDate.toString() !== 'Invalid Date' &&
          endDate.toString() !== 'Invalid date' &&
          startDate < endDate &&
          km >= 0 &&
          calories >= 0;
      }

      // Call Service save

      if (!isValidRequest) {
        return next(generateError(404, 1001));
      }

      // get service
      const Service: IAppService = req.app.get('services');

      const run = await Service.run.saveRun(startDate, endDate, km, calories);

      return res.status(201).json(run);
    }
  );
  return routeur;
};
