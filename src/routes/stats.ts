import { generateError } from '../helpers';
import createDebug from 'debug';
import {
  IAppRequest,
  IAppResponse,
  IAppRouteur,
  IAppNext,
  IAppService
  //   IAppService
} from '../types';

// @ts-ignore
const debug = createDebug('strava:stats:routes');

export default (routeur: IAppRouteur): IAppRouteur => {
  routeur.get(
    '/',
    async (req: IAppRequest, res: IAppResponse, next: IAppNext) => {
      const params = req.query;

      //   check mandatory params
      if (!params.startDate || !params.endDate) {
        return next(generateError(404, 2001));
      }
      const startDate = new Date(params.startDate);
      const endDate = new Date(params.endDate);

      if (
        startDate.toString() === 'Invalid Date' ||
        endDate.toString() === 'Invalid Date'
      ) {
        return next(generateError(404, 2001));
      }

      //   get service
      const Services: IAppService = req.app.get('services');

      const runs = await Services.run.findRuns(
        startDate.toString(),
        endDate.toString()
      );
      const stats = await Services.run.generateRunStats(runs);

      res.status(200).json(stats);
    }
  );
  return routeur;
};
