import createDebug from 'debug';
import { IAppRequest, IAppResponse, IAppNext, ISystemError } from '../types';
import { generateError } from '../helpers';

const debug = createDebug('strava:route:main');

export const indexRoute = (
  req: IAppRequest,
  res: IAppResponse,
  next: IAppNext
) => {
  debug('etonnant');
  return res.status(200).json({
    app: 'strava',
    isOn: true
  });
};

export const notFoundMiddleware = (
  req: IAppRequest,
  res: IAppResponse,
  next: IAppNext
) => {
  next(generateError(501));
};

export const errorMiddleware = (
  err: ISystemError,
  req: IAppRequest,
  res: IAppResponse,
  next: IAppNext
) => {
  debug('ERRRRRr: ', err);
  res.status(err.status).json({
    status: err.status,
    message: err.message,
    code: err.code
  });
};
